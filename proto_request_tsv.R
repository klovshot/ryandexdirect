proto_request_tsv <- function(method, payload, account, FUN, proxy_usage = F, errors = T) {
  
  require(httr)
  require(jsonlite)
  source("~/R/ryandexdirect/get_yandex_token.R")
  
  request_url <- paste0("https://api.direct.yandex.com/json/v5/", method)
  
  
  
  responce_status_code <- 0
  retry_in <- 0
  
  while(responce_status_code != 200) {
    if(responce_status_code %in% 201:202) Sys.sleep(retry_in)
    x <- FUN(request_url,
             add_headers(
               Authorization = paste("Bearer", get_yandex_token(account)),
               returnMoneyInMicros = "false",
               skipReportHeader = "true",
               skipReportSummary = "true"
             ),
             content_type_json(),
             body = payload
    )
    responce_status_code <- status_code(x)
    if(responce_status_code >= 300) stop(paste(unlist(fromJSON(content(x, "text"))), collapse = " "))
    retry_in <- x$headers$retryin
    
  }
  
  x <- content(x, "parsed", "text/tab-separated-values", encoding = "UTF-8")
  x
}
